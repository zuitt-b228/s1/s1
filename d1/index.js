// console.log("hello")

//Global Objects

// Arrays
let students = [
	"Tony",
	"Peter",
	"Wanda",
	"Vision",
	"Loki"
];

console.log(students);

/*
 	What is the difference between .splice and .slice method?
		 .splice() - removes an element from a specified index and adds elements (mutator method)
		 .slice() - copies a portion of the from the starting index (non-mutator method)

	
	What is the other type of array methods?
		iteration methods - loop over items of an array

		forEach() - loops over items in an array and repeats a user-defined function
		map() - loops over items in an array and repeats a user-defined function AND returns a new array
		every() - loops and checks if all items satisfy a given condition

 */

let arrNum = [15,20,25,30,11];

/*
	Mini-activity
		using forEach() method, check if every item in the array is divisible by 5
		if they are, log in the console "<num> is divisible by 5"
		if not, log false
*/

/*arrNum.forEach(

	element => {
		if (element % 5 == 0){
			console.log(element + " is divisble by 5")
		} else {
			console.log(false)
		}
	}
)
*/
// can forEach() alone return data that will tell us if ALL numbers/items in the array is divisible by 5?

/*let divisbleby5 = arrNum.every( num => {
	console.log(num);
	return num % 5 === 0;
});

console.log(divisbleby5);
*/

// Math

/*
	Mathematical Constants
	- 8 pre-defined properties which can be call via the syntax "Math.property" (properties are case-sensitive)
	- global object
	
	console.log(Math);
	console.log(Math.E);		// Euler's number
	console.log(Math.PI);		// PI
	console.log(Math.SQRT2);	// square root of 2
	console.log(Math.SQRT1_2);	// square root of 1/2
	console.log(Math.LN2);		// natural logarithm of 2
	console.log(Math.LN10);		// natural logarithm of 10
	console.log(Math.LOG2E);	// base 2 of logarithm of E
	console.log(Math.LOG10E);	// base 10 of logarithm of 10


	- methods for rounding a number to an integer
	console.log(Math.round(Math.PI));	// rounds to a nearest integer
	console.log(Math.ceil(Math.PI));	// rounds UP to a nearest integer
	console.log(Math.floor(Math.PI));	//	rounds DOWN to a nearest integer
	console.log(Math.trunc(Math.PI));	// returns only the integer part (ES6 update)

	// returns a square root of a number
	console.log(Math.sqrt(3.14));

	// lowest value in a list of arguments
	console.log(Math.min(-1, -2, -4, 0, 1, 2, 3, 4, -3));

	//highest values in a list of arguments
	console.log(Math.max(-1, -2, -4, 0, 1, 2, 3, 4, -3));
*/

// =============================================== //

// 1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.

function addToEnd(array, element){

	if (typeof(element) === "string"){
		array.push(element);
		return array;
	} else {
		return "error - can only add strings to an array";
	}
}

// 2. Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing. 

function addToStart(array, element){

	if (typeof(element) === "string"){
		array.unshift(element);
		return array;
	} else {
		return "error - can only add strings to an array";
	}
}


// 3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.

function elementChecker(array, element){
	if (array.length < 1) {
		return "error - array must NOT be empty";	
	} else {
		return array.some(value => value === element);
	}

}


// 4.    Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:
/*
	if array is empty, return "error - array must NOT be empty"
	*** if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
	*** if every element in the array ends in the passed in character, return true. Otherwise return false.
	Use the students array and the character "e" as arguments when testing.
*/

function checkAllStringsEnding(array, character){
	if (array.length < 1) {
		return "error - array must NOT be empty";	
	}
	if !(array.some(element => typeof(element)==="string")){
		return "error - all array elements must be strings"
	}
	if (typeof(character) != "string") {
		return "error - 2nd argument must be of data type string";
	} 
	if (character.length > 1) {
		return "error - 2nd argument must be a single character"
	} 
	return array.every(element => element[element.length -1] === character)

}


// 5.    Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.

function stringLengthSorter (array) {
	
	if (array.every (element => typeof(element)==="string")){
		return array.sort()
	} else {
		return "error - all array elements must be strings"
	}
}

// 6.    Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:
/*
	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
	*** return the number of elements in the array that start with the character argument, must be case-insensitive
	Use the students array and the character "J" as arguments when testing.
*/

function startsWithCounter(array, character){
	if (array.length < 1) {
		return "error - array must NOT be empty";	
	}
	if (array.some (element => typeof(element)==="string")){
		return "error - all array elements must be strings"
	}
	if (typeof(character) != "string") {
		return "error - 2nd argument must be of data type string";
	} 
	if (character.length > 1) {
		return "error - 2nd argument must be a single character"
	} 

	let counter =0;
	array.forEach(element => {
		if(element[0].toLowerCase() === character.toLowerCase());
			counter++;
	})
	return counter;
}


// 7.    Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:
/*
	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	return a new array containing all elements of the array argument that have the string argument in it, must be case-insensitive
	Use the students array and the character "jo" as arguments when testing.
*/


function likeFinder(array, string){
	if (array.length < 1) {
		return "error - array must NOT be empty";	
	}
	if (array.some (element => typeof(element)==="string")){
		return "error - all array elements must be strings"
	}
	if (typeof(string) != "string") {
		return "error - 2nd argument must be of data type string";
	} 
	if (string.length > 1) {
		return "error - 2nd argument must be a single character"
	} 

}


// 8.    Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.

function randomPicker(array){
	var random = Math.floor(Math.random() * array.length);
	return array[random]
}















