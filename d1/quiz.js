/*

1 How do you create arrays in JS?
2 How do you access the first character of an array?
3 How do you access the last character of an array?
4 What array method searches for, and returns the index of a given value in an array? 
5 What array method loops over all elements of an array, performing a user-defined function on each iteration?
6 What array method creates a new array with elements obtained from a user-defined function?
7 What array method checks if all its elements satisfy a given condition?
8 What array method checks if at least one of its elements satisfies a given condition?
9 True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
10 True or False: array.slice() copies elements from original array and returns these as a new array.


*/

// 1 How do you create arrays in JS?
	let myArray = [1,2,3,4,5];

// 2 How do you access the first character of an array?
	console.log(myArray[0]);
	console.log(myArray.slice(0,1));

// 3 How do you access the last character of an array?
	last = myArray.length -1
	console.log(myArray[last]);
	console.log(myArray.slice(-1));

// 4 What array method searches for, and returns the index of a given value in an array? 
	console.log(myArray.indexOf(3));

// 5 What array method loops over all elements of an array, performing a user-defined function on each iteration?
	myArray.forEach();

// 6 What array method creates a new array with elements obtained from a user-defined function?
	myArray.map();

// 7 What array method checks if all its elements satisfy a given condition?
	myArray.every();

// 8 What array method checks if at least one of its elements satisfies a given condition?
	myArray.some();

// 9 True or False: array.splice() modifies a copy of the array, leaving the original unchanged. 
	FALSE

// 10 True or False: array.slice() copies elements from original array and returns these as a new array.
	TRUE

